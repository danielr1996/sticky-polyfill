module.exports = function(grunt){
    grunt.initConfig({
        uglify : {
            options : {
                banner : "/*! app.min.js file */\n"
            },
            build : {
                src : ["src/sticky-polyfill.js"],
                dest : "dist/sticky-polyfill.min.js"
            }

        },
        exec:{
            publish: 'npm publish --access=public'
        }

    });
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-exec');

    grunt.registerTask('pub', 'exec:publish');

    grunt.registerTask('build', ['uglify']);

    grunt.registerTask('default', ['build']);
}
