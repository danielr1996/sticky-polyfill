# About
This library is a VanillaJS polyfill for the CSS Attribute `position: sticky`
# Usage
## Angular (Angular2+)
First include the script in .angular-cli.json so it gets added to your build with `ng build` ...
```
{ "glob": "**/*", "input": "../node_modules/@danielr1996/sticky-polyfill/dist", "output": "./assets/js/sticky-polyfill/" }
```

... and include the script in `index.html`
```
<script src="/assets/js/sticky-polyfill/sticky-polyfill.min.js"></script>
```

Then you need to call the sticky-polyfill library after the desired element is initialized (AfterViewInit hook in AppComponent should always work, but you can also choose the component where your desired element is used).
You can use any valid CSS Selector (https://developer.mozilla.org/de/docs/Web/API/Document/querySelector);
```
export class AppComponent implements AfterViewInit {
  ngAfterViewInit(): void {
    sticky('nav');
  }
}
```

and add this to the top of your component so TypeScript doesn't complain that `sticky` is unknown (it is added to the `window` object) 
```
declare var sticky;
``` 
